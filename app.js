const express = require('express');
const morgan = require('morgan');

const globalErrorHandler = require('./src/controllers/errorController');
const AppError = require('./src/utils/appError');

const dishesRouter = require('./src/routes/dishes');

// Start express app
const app = express();

// Development logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(express.json());

// 3) ROUTES
app.use('/api/v1/dishes', dishesRouter);

app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});

app.use(globalErrorHandler);

module.exports = app;
