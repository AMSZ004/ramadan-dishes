const catchAsync = require('../utils/catchAsync');
const fetch = require('node-fetch');
const fs = require('fs');
const AppError = require('../utils/appError');

exports.cooktime = catchAsync(async (req, res, next) => {
  const ingredient = req.query.ingredient;
  let day = req.query.day;

  if (!ingredient || !day) {
    return next(new AppError('Please provide the day and the ingredient', 400));
  }

  if (day < 1 || day > 30) {
    return next(new AppError('Please enter a valid day', 401));
  }

  if (day.length == 1) {
    day = 0 + day;
  }
  //   getting prayer times from api
  const response = await fetch(
    `http://api.aladhan.com/v1/hijriCalendar?latitude=35.829300&longitude=10.640630&annual=true&method=1`
  );
  const prayerTimes = await response.json();

  //getting next ramadhan
  let ramadhan = [];
  for (const property in prayerTimes.data) {
    let month = prayerTimes.data[property];
    const day = month.filter(el => el.date.hijri.month.ar === 'رَمَضان');
    if (day.length > 0) {
      ramadhan = ramadhan.concat(day);
    }
  }

  // getting the ramadhan day
  let ramadhanDay = [];
  ramadhanDay = ramadhan.filter(el => el.date.hijri.day == day);

  //   reading the dishes json file
  const dishesObj = fs.readFileSync(
    `${__dirname}/../data/dishes.json`,
    'utf-8'
  );
  let dishes = JSON.parse(dishesObj);

  // lower casing the ingredients to use the query
  dishes = dishes.map(el => {
    return {
      ...el,
      ingredients: el.ingredients.map(el => el.toLowerCase())
    };
  });

  // filtering the dishes with the query
  dishes = dishes.filter(dish =>
    dish.ingredients.includes(req.query.ingredient)
  );

  // getting the time right
  let maghrebTime =
    Math.floor(ramadhanDay[0].timings.Maghrib.slice(3, 5)) +
    60 * Math.floor(ramadhanDay[0].timings.Maghrib.slice(0, 2));

  let asrTime =
    Math.floor(ramadhanDay[0].timings.Asr.slice(3, 5)) +
    60 * Math.floor(ramadhanDay[0].timings.Asr.slice(0, 2));

  // adding the cooktime key to our dishes
  dishes.map(el => {
    let difference = 15 + el.duration;
    let cooktime = maghrebTime - (asrTime + difference);
    delete el.duration;
    if (cooktime > 0) {
      el.cooktime = `${cooktime} Minutes after Asr`;
    } else {
      el.cooktime = `${Math.abs(cooktime)} Minutes before Asr`;
    }
    return el;
  });

  res.status(200).json({
    dishes
  });
});

exports.suggest = catchAsync(async (req, res, next) => {
  let day = req.query.day;
  // validation
  if (!day) {
    return next(new AppError('Please provide the day', 400));
  }

  if (day < 1 || day > 30) {
    return next(new AppError('Please enter a valid day', 401));
  }

  if (day.length == 1) {
    day = 0 + day;
  }

  //   reading the dishes json file
  const dishesObj = fs.readFileSync(
    `${__dirname}/../data/dishes.json`,
    'utf-8'
  );
  let dishes = JSON.parse(dishesObj);

  //   getting prayer times from api
  const response = await fetch(
    `http://api.aladhan.com/v1/hijriCalendar?latitude=35.829300&longitude=10.640630&annual=true&method=1`
  );
  const prayerTimes = await response.json();

  //getting next ramadhan
  let ramadhan = [];
  for (const property in prayerTimes.data) {
    let month = prayerTimes.data[property];
    const day = month.filter(el => el.date.hijri.month.ar === 'رَمَضان');
    if (day.length > 0) {
      ramadhan = ramadhan.concat(day);
    }
  }

  // getting the ramadhan day
  let ramadhanDay = [];
  ramadhanDay = ramadhan.filter(el => el.date.hijri.day == day);

  // getting the time right
  let maghrebTime =
    Math.floor(ramadhanDay[0].timings.Maghrib.slice(3, 5)) +
    60 * Math.floor(ramadhanDay[0].timings.Maghrib.slice(0, 2));

  let asrTime =
    Math.floor(ramadhanDay[0].timings.Asr.slice(3, 5)) +
    60 * Math.floor(ramadhanDay[0].timings.Asr.slice(0, 2));

  dishes.map(el => {
    let difference = 15 + el.duration;
    let cooktime = maghrebTime - (asrTime + difference);
    delete el.duration;
    if (cooktime > 0) {
      el.cooktime = `${cooktime} Minutes after Asr`;
    } else {
      el.cooktime = `${Math.abs(cooktime)} Minutes before Asr`;
    }
    return el;
  });

  // selecting a random dish to suggest
  let dish = dishes[Math.floor(Math.random() * dishes.length)];

  res.status(200).json({
    data: dish
  });
});
