const express = require('express');
const { cooktime, suggest } = require('../controllers/dishes');
const router = express.Router();

router.route('/cooktime').get(cooktime);
router.route('/suggest').get(suggest);

// router
//   .route('/:id')
//   .put(updateAdmin)
//   .delete(deleteAdmin)
//   .get(getOneAdmin);

module.exports = router;
