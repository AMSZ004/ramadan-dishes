welcome to my humble api for ramadan meals 😃

for the first route : 🥇
provide an ingredient and a day of ramadhan to get the meals with these ingredients and when to cook them depending on the Asr prayer time ⏲️.
Example : http://localhost:3000/api/v1/dishes/cooktime?ingredient=onion&day=13

for the second route : 🥈
provide a day of ramdhan and the api will suggest you a meal a random meal with the cooking time ⏲️ depending od the Asr prayer of that day 🕌.
Example : http://localhost:3000/api/v1/dishes/suggest?day=24

Please create a .env file on the project with these fields to get the full awesome experience of this api 😄 :
NODE_ENV = development
PORT = 3000
